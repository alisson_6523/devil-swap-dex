import { useEffect, useState } from 'react'
import retry from 'async-retry'
import axios from 'axios'
import BigNumber from 'bignumber.js'

/* eslint-disable camelcase */
export interface DeBankTvlResponse {
  id: string
  chain: string
  name: string
  site_url: string
  logo_url: string
  has_supported_portfolio: boolean
  tvl: number
}


// devil 0x9e440a11C77E0888018ff904037cc10bD14fE1bA

export const useGetStats = () => {
  const [data, setData] = useState<DeBankTvlResponse | null>(null)

  useEffect(() => {

    // async function fetch() {
    //   const price_feed = await retry(async bail => axios.get('https://api.coingecko.com/api/v3/simple/price?ids=wbnb&vs_currencies=usd&include_market_cap=true&include_24hr_vol=true&include_24hr_change=true'))
    //   const response = await retry(async bail => axios.get('https://api.bscscan.com/api?module=stats&action=tokenCsupply&contractaddress=0x9e440a11C77E0888018ff904037cc10bD14fE1bA&apikey=9243MX8AKTSF5M8JUK12QVUQMPARDJ24FS'))
    //   const total = parseInt(new BigNumber(response.data.result).div(10 ** 8).toFixed(2));
    //   console.log(price_feed)
    //   console.log(response)
    //   console.log(total * price_feed.data.wbnb.usd)
    //   // return (total * price_feed.data.bitcoin.usd);
    // }

    const fetchData = async () => {
      try {
        const chain = "bsc"
        const has_supported_portfolio = true
        const id = 'devilswap'
        const logo_url = 'https://kind-torvalds-ed864a.netlify.app/images/devil.png'
        const name = "DevilSwap"
        const site_url = 'https://kind-torvalds-ed864a.netlify.app/'

        // const response = await fetch('https://openapi.debank.com/v1/protocol?id=bsc_pancakeswap')

        const price_feed = await retry(async () => axios.get('https://api.coingecko.com/api/v3/simple/price?ids=wbnb&vs_currencies=usd&include_market_cap=true&include_24hr_vol=true&include_24hr_change=true'))
        const resultado = await retry(async () => axios.get('https://api.bscscan.com/api?module=stats&action=tokenCsupply&contractaddress=0xdcf4ed11a2eb149ebb685803a62c327834bfe377&apikey=9243MX8AKTSF5M8JUK12QVUQMPARDJ24FS'))
        const total = parseInt(new BigNumber(resultado.data.result).div(10 ** 9).toFixed(2));

        const tvl = total * price_feed.data.wbnb.usd

        // const responseData: DeBankTvlResponse = await response.json()

        setData({chain, has_supported_portfolio, id, logo_url, name, site_url, tvl})
      } catch (error) {
        console.error('Unable to fetch data:', error)
      }
    }

    fetchData()

    // fetch()
  }, [setData])

  return data
}
