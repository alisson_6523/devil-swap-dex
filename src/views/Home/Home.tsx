import React from 'react'
import styled from 'styled-components'
import { Heading, Text, BaseLayout } from '@pancakeswap/uikit'
import { useTranslation } from 'contexts/Localization'
import Page from 'components/layout/Page'
import FarmStakingCard from 'views/Home/components/FarmStakingCard'
import CakeStats from 'views/Home/components/CakeStats'
import TotalValueLockedCard from 'views/Home/components/TotalValueLockedCard'
import EarnAPRCard from 'views/Home/components/EarnAPRCard'
import EarnAssetCard from 'views/Home/components/EarnAssetCard'
import PredictionPromotionCard from './components/PredictionPromotionCard'
import WinCard from './components/WinCard'

const Hero = styled.div`
  align-items: center;  
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin: auto;
  margin-bottom: 32px;
  padding-top: 116px;
  text-align: center;
  position: relative;
  .title{
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  img{
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  ${({ theme }) => theme.mediaQueries.lg} {
    background-position:center;
    height: 220px;
    padding-top: 0;
  }

  @media(max-width: 768px){
    padding: 0px;
    margin: 0px;
    margin-bottom: 16px;
    height: 180px;
    img{
      object-fit: cover;
      height: 170px;
      object-position: left;
    }
    .title{
      width: 100%;

    }
  }

  @media(max-width: 480px){
    .text{
      display: none;
    }
  }
`

const Cards = styled(BaseLayout)`
  align-items: stretch;
  justify-content: stretch;
  margin-bottom: 24px;
  grid-gap: 24px;

  & > div {
    grid-column: span 6;
    width: 100%;
  }

  ${({ theme }) => theme.mediaQueries.sm} {
    & > div {
      grid-column: span 8;
    }
  }

  ${({ theme }) => theme.mediaQueries.lg} {
    margin-bottom: 32px;
    grid-gap: 32px;

    & > div {
      grid-column: span 6;
    }
  }
`

const CTACards = styled(BaseLayout)`
  align-items: start;
  margin-bottom: 24px;
  grid-gap: 24px;

  & > div {
    grid-column: span 6;
  }

  ${({ theme }) => theme.mediaQueries.sm} {
    & > div {
      grid-column: span 8;
    }
  }

  ${({ theme }) => theme.mediaQueries.lg} {
    margin-bottom: 32px;
    grid-gap: 32px;

    & > div {
      grid-column: span 4;
    }
  }
`

const Home: React.FC = () => {
  const { t } = useTranslation()

  return (
    <Page>
      <Hero>
        <img src="/images/pan-bg-mobile.png" alt="" />
        <div className="title">
          <Heading as="h1" scale="xl" mb="24px" color="white; text-shadow: yellow 0.1em 0.2em 0.2em">
            {t('DevilSwap')}
          </Heading>
          <Text className="text" color="white; text-shadow: yellow 0.1em 0.2em 0.2em">{t('The #1 AMM and yield farm on Binance Smart Chain.')}</Text>
        </div>
      </Hero>
      <div>
        <Cards>
          <FarmStakingCard />
          <PredictionPromotionCard />
        </Cards>
        <CTACards>
          <EarnAPRCard />
          <EarnAssetCard />
          <WinCard />
        </CTACards>
        <Cards>
          <CakeStats />
          <TotalValueLockedCard />
        </Cards>
      </div>
    </Page>
  )
}

export default Home
