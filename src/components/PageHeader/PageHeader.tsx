import React from 'react'
import styled from 'styled-components'
import { Box } from 'devilswap-uikit'
import Container from '../layout/Container'
import banner from '../../assets/img/banner-farm.png'

const Outer = styled(Box)<{ background?: string }>`
  background-image: url(${banner});
  background-repeat: no-repeat;
  background-size: cover;
`

const Inner = styled(Container)`
  padding-top: 32px;
  padding-bottom: 32px;
`

const PageHeader: React.FC<{ background?: string }> = ({ background, children, ...props }) => (
  <Outer background={background} {...props}>
    <Inner>{children}</Inner>
  </Outer>
)

export default PageHeader
