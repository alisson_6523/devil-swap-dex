import { MenuEntry } from 'devilswap-uikit'
import { ContextApi } from 'contexts/Localization/types'

const config: (t: ContextApi['t']) => MenuEntry[] = (t) => [
  {
    label: t('Home'),
    icon: 'HomeIcon',
    href: '/',
  },
  {
    label: t('Trade'),
    icon: 'TradeIcon',
    items: [
      {
        label: t('Exchange'),
        href: 'https://exchange.pancakeswap.finance/#/swap?outputCurrency=0x9e440a11C77E0888018ff904037cc10bD14fE1bA',
      },
      {
        label: t('Liquidity'),
        href: 'https://exchange.pancakeswap.finance/#/pool',
      },
      
    ],
  },
  {
    label: t('Farms'),
    icon: 'FarmIcon',
    href: '/farms',
  },
  {
    label: t('Pools'),
    icon: 'PoolIcon',
    href: '/pools',
  },
  
  {
    label: t('Lottery (Coming soon)'),
    icon: 'TicketIcon',
    href: '/#',
  },
  {
    label: t('NFT Store (Coming soon)'),
    icon: 'NftIcon',
    href: '/#',
  },
  
  {
    label: t('More'),
    icon: 'MoreIcon',
    items: [
      {
        label: t('Contact'),
        href: 'https://docs.pancakeswap.finance/contact-us',
      },
      {
        label: t('Github'),
        href: 'https://github.com/devilswap',
      },
      {
        label: t('Docs'),
        href: 'https://devilswap.gitbook.io/devilswap',
      },
      {
        label: t('Blog'),
        href: 'https://medium.com/',
      },
      {
        label: t('Merch (Coming soon)'),
        href: '#',
      },
    ],
  },
]

export default config
